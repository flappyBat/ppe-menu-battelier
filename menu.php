<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Affichage du Menu</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  </head>
  <body>
    <div class="">
      HEADER A METTRE
    </div>
    <div class="container-fluid">
    <div class="row" id="afficheRepas">

    </div>
  </div>
  </body>
</html>

<script type="text/javascript">

var http = new XMLHttpRequest();
var divParentToPrintContent = document.getElementById('afficheRepas');
let jsonUrl = "php/getmenu2.php";

window.onload = function(){
  request(jsonUrl,parseJson,null);
}
// param one : url to call,
// second param : function to call when the data is ready
function request(url1,call,params){
  http.open('POST', url1, true);

  //Send the proper header information along with the request
  http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

  http.onreadystatechange = function() {//Call a function when the state changes.
      if(http.readyState == 4 && http.status == 200 && typeof call === "function") {
          call(http.responseText);
      }
  }

  http.send(params);

}

function parseJson(json){
  let baseJson = JSON.parse(json);
  let daysOfTheWeek= ["lundi","mardi","mercredi","jeudi","vendredi"];
  // console.log(baseJson);
  for (var i = 0; i < daysOfTheWeek.length; i++) {
    let container = document.createElement("div");
    if(i == 0){
      container.classList.add("col-lg-2","offset-lg-1","col-md-2","md-offset-2","border","border-secondary");
    }else{
      container.classList.add("col-lg-2","col-md-2","md-offset-2","border","border-secondary");

    }

    let day = document.createElement("h1");
    day.appendChild(document.createTextNode(daysOfTheWeek[i]));
    container.appendChild(day);

    // repas du midi ou soir
    let midiOuLeSoir = Object.keys(baseJson[daysOfTheWeek[i]]);
      for (var wq = 0; wq < midiOuLeSoir.length; wq++) {
        console.log(midiOuLeSoir[wq]);
        let nodeRepas = document.createElement("p");
        nodeRepas.appendChild(document.createTextNode(midiOuLeSoir[wq]));
        container.appendChild(nodeRepas);

      // plat entree dessert etc..
        let keysOfTypeRepas = Object.keys(baseJson[daysOfTheWeek[i]][midiOuLeSoir[wq]]);
      // englobe tous plat entree dessert
        let typeRepas = baseJson[daysOfTheWeek[i]][midiOuLeSoir[wq]];

        console.log("avant affichage des articles");
        // parcours tous les types entree plat dessert
        for (var ap = 0; ap < keysOfTypeRepas.length; ap++) {
            let containerTypeRepas = document.createElement("div");
            container.appendChild(containerTypeRepas);
            let allProducts = typeRepas[keysOfTypeRepas[ap]];
            // On parcours tous les produits pour un type donnée : entree  dessert..
            for (var wap = 0; wap < allProducts.length; wap++) {
              console.log("designation du produit : " + allProducts[wap].designation);
              let currentRepas = document.createElement("p");
              currentRepas.appendChild(document.createTextNode(allProducts[wap].designation));
              containerTypeRepas.appendChild(currentRepas);
            }

        }

    }
    divParentToPrintContent.appendChild(container);
  }

}

</script>
