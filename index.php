<?php
include('php/connectBDD.php');
include('php/permission.php');
if (!empty($_POST['login']) && !empty($_POST['password']) ) {
  // $query = "select login,password FROM user_member where login = ? and password = ? INNER JOIN user_permission
  //           ON user_member.login = user_permission.member";
  $query = "select login,user_permission.permission FROM user_member LEFT JOIN user_permission
            ON user_member.login = user_permission.member where login = ? and password = ?";

  $result =  $bdd->prepare($query);
  $result->bindParam(1, $_POST['login'],PDO::PARAM_STR);
  $result->bindParam(2, md5($_POST['password']),PDO::PARAM_STR);
  $result->execute();

  if ($result->rowCount() == 0) {
    $_POST['error'] = "Le mot de passe ou l'identifiant n'est pas le bon";
  }else{
    session_start();
    $_SESSION['permission']['admin'] = 0;
    $_SESSION['permission']['menu'] = 0;

    while ($row = $result->fetch()) {
      if ($row["permission"] == "admin") {
        $_SESSION['permission']['admin'] = 1;
      }elseif ($row["permission"] == "menu") {
        $_SESSION['permission']['menu'] = 1;
      }
    }

    header("Location: choix.php");
  }
}

 ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Page de Connexion </title>
    <link rel="stylesheet" href="css/login.css">
  </head>
  <body>

    <main>
      <h1>Formulaire de connexion</h1>
      <form class="" action="index.php" method="post">
        <input type="text" name="login" value="" placeholder="Login" required>
        <input type="password" name="password" value="" placeholder="Password" required>
        <input type="submit" name="" value="Validation">
        <?php
          if (isset($_POST['error'])) {
            echo $_POST['error'];
            $_POST['error'] = null;
          }
         ?>
      </form>
    </main>

  </body>
</html>
