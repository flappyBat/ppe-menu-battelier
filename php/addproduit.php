<?php
header('Access-Control-Allow-Origin: *');
function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
$reponce = array('msg' => "Ajout reussi", 'success' => true);
$date_repas = $_POST["date"];
$day =  date('D', strtotime($date_repas));
if ($day == "Mon") {
	$date_semaine = $date_repas;
	$day = 0;
}else if($day == "Sat"){
	$reponce['msg'] = "Pas de repas le samedi";
	$reponce['success'] = false;
}else if($day == "Sun"){
	$reponce['msg'] = "Pas de repas le dimanche";
	$reponce['success'] = "false";
}else{
	$date_semaine = date('Y-m-d',strtotime("last Monday",strtotime($date_repas)));
	$datetime1 = new DateTime($date_semaine);
	$datetime2 = new DateTime($date_repas);
	$interval = $datetime1->diff($datetime2);
	$day = $interval->format('%a');
}
if ($reponce['success']) {
	try {
		include('connectBDD.php');
		$prepared = $bdd->prepare("INSERT IGNORE INTO semaine(`date`, `duree`) VALUES (?, 5)");
		$prepared->execute(array($date_semaine));
		$prepared = $bdd->prepare("INSERT IGNORE INTO repas(`date_semaine`, `jour`, `dejeuner`) VALUES (?, ?, ?)");
		$prepared->execute(array($date_semaine, $day, $_POST['dejeuner']));
		$prepared = $bdd->prepare("INSERT INTO produit(`id_repas`, `designation`, `type`) VALUES ( (SELECT id FROM repas WHERE date_semaine = ? AND jour = ? AND dejeuner = ? LIMIT 1), ?, ?)");
		$prepared->execute(array($date_semaine, $day, $_POST['dejeuner'], $_POST['designation'], $_POST['type']));
	} catch (Exception $e) {
		$reponce['msg'] = $e->getMessage();
		$reponce['success'] = false;
	}
}
echo json_encode($reponce, JSON_UNESCAPED_UNICODE);
?>
