<?php
session_start();
include("php/permission.php");

if (!hasPermission()) {
  header("Location: index.php");
}

 ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container-fluid">


    <div class="row justify-content-center">

      <div class="col-lg-2">
        <div class="reponse">

        </div>

        <input type="date" name="date" value="" id="date">

        <select class="" name="" id="type_repas">
          <option value="dejeuner">dejeuner</option>
          <option value="diner">Diner</option>
        </select>

        <input type="text" name="designation" value="" placeholder="designation" id="designation">

        <select class="" name="" id="type_produit">
          <option value="accompagnement">accompagnement</option>
          <option value="entree">entree</option>
          <option value="plat">plat</option>
          <option value="laitage">laitage</option>
          <option value="dessert">dessert</option>
        </select>

        <input type="hidden" name="bio" value="0" id="bio">
        <input type="hidden" name="local" value="0" id="local">
        <input type="hidden" name="maison" value="0" id="maison">
        <input type="hidden" name="frais" value="0" id="frais">

        <button type="button" name="button" onclick="ajout()" value="">Validation</button>


      </div>
    </div>

    <div class="row" id="content">

    </div>
    </div>
  </body>
</html>

<script type="text/javascript">
var http = new XMLHttpRequest();

let reponse = document.getElementsByClassName('reponse')[0];


function ajout(){
  let date = "date=" +  document.getElementById('date').value;
  let type_repas = "&type_repas=" + document.getElementById('type_repas').value;
  let designation = "&designation=" + document.getElementById('designation').value;
  let type_produit = "&type_produit=" + document.getElementById('type_produit').value;
  let bio = "&bio=" + document.getElementById('bio').value;
  let local = "&local=" + document.getElementById('local').value;
  let maison = "&maison=" + document.getElementById('maison').value;
  let frais = "&frais=" +document.getElementById('frais').value;


  let params = date + type_repas + designation + type_produit + bio + local + maison + frais;
  console.log(params);
  request("php/addproduit2.php",printResponse,params,null);


}

function printResponse(text,div){
  console.log("IN RESPONSE");
  text = JSON.parse(text);
  reponse.innerHTML = text["msg"];

  let divToApply = document.getElementById('content');

  while(divToApply.firstChild){
    divToApply.removeChild(divToApply.firstChild);
  }

  //  A MODIFIER POUR AJOUTER LA DATE DONT LE MEC VIENS DE PASSER EN PARAMETRE
  request("php/getmenu2.php",parseJson,null,divToApply);
}

function request(url1,call,params,div){
  http.open('POST', url1, true);
  console.log("IN");
  //Send the proper header information along with the request
  http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

  http.onreadystatechange = function() {//Call a function when the state changes.
      if(http.readyState == 4 && http.status == 200 && typeof call === "function") {
          console.log("REPONSE");
          call(http.responseText,div);
      }
  }

  http.send(params);

}

function parseJson(json,divParentToPrintContent){
  let baseJson = JSON.parse(json);
  let daysOfTheWeek= ["lundi","mardi","mercredi","jeudi","vendredi"];
  // console.log(baseJson);
  for (var i = 0; i < daysOfTheWeek.length; i++) {
    let container = document.createElement("div");
    if(i == 0){
      container.classList.add("col-lg-2","offset-lg-1","col-md-2","md-offset-2","border","border-secondary");
    }else{
      container.classList.add("col-lg-2","col-md-2","md-offset-2","border","border-secondary");

    }

    let day = document.createElement("h1");
    day.appendChild(document.createTextNode(daysOfTheWeek[i]));
    container.appendChild(day);

    // repas du midi ou soir
    let midiOuLeSoir = Object.keys(baseJson[daysOfTheWeek[i]]);
      for (var wq = 0; wq < midiOuLeSoir.length; wq++) {
        console.log(midiOuLeSoir[wq]);
        let nodeRepas = document.createElement("p");
        nodeRepas.appendChild(document.createTextNode(midiOuLeSoir[wq]));
        container.appendChild(nodeRepas);

      // plat entree dessert etc..
        let keysOfTypeRepas = Object.keys(baseJson[daysOfTheWeek[i]][midiOuLeSoir[wq]]);
      // englobe tous plat entree dessert
        let typeRepas = baseJson[daysOfTheWeek[i]][midiOuLeSoir[wq]];

        console.log("avant affichage des articles");
        // parcours tous les types entree plat dessert
        for (var ap = 0; ap < keysOfTypeRepas.length; ap++) {
            let containerTypeRepas = document.createElement("div");
            container.appendChild(containerTypeRepas);
            let allProducts = typeRepas[keysOfTypeRepas[ap]];
            // On parcours tous les produits pour un type donnée : entree  dessert..
            for (var wap = 0; wap < allProducts.length; wap++) {
              console.log("designation du produit : " + allProducts[wap].designation);
              let currentRepas = document.createElement("p");
              currentRepas.appendChild(document.createTextNode(allProducts[wap].designation));
              containerTypeRepas.appendChild(currentRepas);
            }

        }

    }
    divParentToPrintContent.appendChild(container);
  }

}





</script>
