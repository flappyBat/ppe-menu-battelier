<?php
session_start();
include("php/permission.php");

if (!hasPermission()) {
  header("Location: index.php");
}

echo $_SESSION['permission']['admin'];
 ?>

 <!DOCTYPE html>
 <html lang="fr" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title></title>
   </head>
   <body>
     <a href="menu.php">Voir le Menu de la semaine</a>
     <?php if ($_SESSION['permission']['admin'] == 1): ?>
       <a href="addMenu.php">Ajouter un menu</a>
     <?php endif; ?>
   </body>
 </html>
